
from .seed import seed


def init(app):
    app.cli.command('seed')(seed)
