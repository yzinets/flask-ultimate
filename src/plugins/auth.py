from flask import request, abort, g

from src import models


class Auth:

    def init_app(self, app):
        pass

    @staticmethod
    def require_token(f):
        def wrapper(*args, **kwargs):
            auth = request.headers.get('Authorization')
            # Abort if token is not set
            if not auth:
                return abort(401)
            # Auth for bearer token
            if 'Bearer' in auth:
                # Extract from db
                token = auth.replace('Bearer ', '')
                token = models.Token.query.filter_by(token=token).first()
                if not token:
                    return abort(401)
                # Extract account
                acc = models.Account.query.\
                    filter_by(id=token.account_id).\
                    first()
                if not acc:
                    return abort(401)
                g.account = acc
                g.token = token
                return f(*args, **kwargs)
            # Default
            else:
                return abort(401)
        return wrapper


auth = Auth()
