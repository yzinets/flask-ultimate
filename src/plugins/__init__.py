
from .sqla import sqla
from .migrate import migrate
from .auth import auth
from .sqltap import sqltap
from .restplus import restplus
from .celery import celery_app


def init(app):
    sqla.init_app(app)
    migrate.init_app(app, db=sqla)
    sqltap.init_app(app)
    celery_app.init_app(app)
    auth.init_app(app)
    restplus.init_app(app)
