from flask import url_for
from flask_restplus import Api

from conf import conf
from src import endpoints


# Configs
authorizations = {
    'apiKey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}


class CustomApi(Api):
    @property
    def specs_url(self):
        """Monkey patch for HTTPS"""
        return url_for(
            self.endpoint('specs'),
            _external=True,
            _scheme=conf.PREFERRED_URL_SCHEME
        )


# Init
restplus = CustomApi(
    title='API',
    version=conf.VERSION,
    description='Real Estate API',
    authorizations=authorizations
)


# Attach namespaces
restplus.namespaces.clear()
for namespace in endpoints.collection.values():
    restplus.add_namespace(namespace)
