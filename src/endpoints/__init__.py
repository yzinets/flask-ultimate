
from .testing import testing


collection = {n.__class__.__name__: n for n in [
    testing
]}
