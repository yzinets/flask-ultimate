from flask_restplus import Resource

from src.tasks.example import example

from . import testing


@testing.route('/ping')
class Ping(Resource):

    def get(self):
        example.delay()
        return 'pong'
