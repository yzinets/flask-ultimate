from flask_restplus import Namespace


# Init namespace
testing = Namespace('Testing', path='/')

# Register serializers
# ...


# Attach resources
from . import Ping  # noqa: F401
