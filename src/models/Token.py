
from src.plugins import sqla


class Token(sqla.Model):
    __tablename__ = 'tokens'

    id = sqla.Column(sqla.Integer, primary_key=True)
    account_id = sqla.Column(sqla.Integer, sqla.ForeignKey('accounts.id'))
    token = sqla.Column(sqla.String)
    token_type = sqla.Column(sqla.String)
