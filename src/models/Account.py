
from src.plugins import sqla

from .mixins.UpdateMixin import UpdateMixin


class Account(sqla.Model, UpdateMixin):
    __tablename__ = 'accounts'

    id = sqla.Column(sqla.Integer, primary_key=True)
    email = sqla.Column(sqla.String, unique=True)
    password = sqla.Column(sqla.String)
