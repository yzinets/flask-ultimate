from flask import Flask
from werkzeug.contrib.profiler import ProfilerMiddleware

from conf import conf
from src import plugins, commands


def create_app():

    # Create basic Flask app
    app = Flask(__name__)

    # Load configs
    app.config.update(conf)
    app.secret_key = conf.SECRET

    # Profiler
    if conf.PROFILE:
        app.wsgi_app = ProfilerMiddleware(
            app.wsgi_app,
            sort_by=('tottime',),
            restrictions=[conf.PROFILE_DEPTH or 30])

    # Init plugins
    plugins.init(app)

    # Init commands
    commands.init(app)

    return app
