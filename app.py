
from src.create_app import create_app
# Celerybeat required
from src.plugins import celery_app  # noqa: F401

app = create_app()

if __name__ == "__main__":
    app.run()
