from addict import Dict
import os

configs = Dict()

configs.update(Dict(
    development=Dict(

        SECRET='asdfsghfjg',
        VERSION='0.1.1',

        DEBUG=True,
        PROFILE=False,
        SQLTAP=False,

        PROFILE_DEPTH=100,
        SQLTAP_PATH='./sqltap.html',

        SQLALCHEMY_DATABASE_URI='postgresql://'
                                'ultimate:ultimate_secret@'
                                'postgres/ultimate',
        SQLALCHEMY_TRACK_MODIFICATIONS=False,

        PREFERRED_URL_SCHEME='http'

    )
))

conf = configs[os.environ.get('FLASK_ENV') or 'development']
