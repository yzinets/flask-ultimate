(
    (flask db init || echo 'Flask DB init is not required') &&
    flask db migrate &&
    flask db upgrade &&
    flask run --host=0.0.0.0
)
